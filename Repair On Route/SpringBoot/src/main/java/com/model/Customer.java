package com.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Entity
public class Customer {

	@Id
	@GeneratedValue
	private int customerId;
	private String firstName;
	private String lastName;
	private String gender;
	@NotNull
	@Column(name = "emailId", unique = true, nullable = false)
	private String emailId;
	@NotNull
	@Column(name = "phoneNumber", unique = true, nullable = false)
	private String phoneNumber;
	private String address;
	private String password;
	private String otp;

	public Customer() {
	}

	public Customer(String firstName, String lastName, String gender, String emailId, String phoneNumber,
			String address, String password, String otp) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.gender = gender;
		this.emailId = emailId;
		this.phoneNumber = phoneNumber;
		this.address = address;
		this.password = password;
		this.otp = otp;
	}
	
	public Customer(int customerId, String firstName, String lastName, String gender, String emailId,
			String phoneNumber, String address, String password) {
		this.customerId = customerId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.gender = gender;
		this.emailId = emailId;
		this.phoneNumber = phoneNumber;
		this.address = address;
		this.password = password;
	}

	public int getCustomerId() {
		return customerId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		if (!phoneNumber.startsWith("+")) {
			this.phoneNumber = "+91" + phoneNumber;
		} else {
			this.phoneNumber = phoneNumber;
		}
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getOtp() {
		return otp;
	}

	public void setOtp(String otp) {
		this.otp = otp;
	}
}
