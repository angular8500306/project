package com.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
@Entity
public class Booking {
	@Id
	@GeneratedValue
	private int bookingId;
	private String emailId;
	private String location;
	private String phoneNumber;
	private String services;
	
	public Booking(){
		super();
	}
	
	public Booking(String emailId, String location, String phoneNumber, String services) {
		this.emailId = emailId;
		this.location = location;
		this.phoneNumber = phoneNumber;
		this.services = services;
	}
	public Booking(int bookingId, String emailId, String location, String phoneNumber, String services) {
		this.bookingId = bookingId;
		this.emailId = emailId;
		this.location = location;
		this.phoneNumber = phoneNumber;
		this.services = services;
	}
	@Override
	public String toString() {
		return "Booking [bookingId=" + bookingId + ", emailId=" + emailId + ", location=" + location + ", phoneNumber="
				+ phoneNumber + ", services=" + services + "]";
	}
	public int getBookingId() {
		return bookingId;
	}
	public void setBookingId(int bookingId) {
		this.bookingId = bookingId;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getServices() {
		return services;
	}
	public void setServices(String services) {
		this.services = services;
	}
}
