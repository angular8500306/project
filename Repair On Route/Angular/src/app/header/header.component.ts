import { Router } from '@angular/router';
import { Component } from '@angular/core';
import { CustomerService } from '../customer.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent {
  isadminloggedin: boolean = false;
  iscustomerLogedin: boolean = false;
  isloggedin: boolean = true;
  isModalOpen: boolean = false;
  cust: any;
  captchaResponse: string = '';
  captchaInvalid: boolean = true
  emailId: any;
  cartItemsLength: number = 0;
  siteKey: string = '6LeYEm8pAAAAAPWqu6jZISB-yllgRN5a44casYWG';
  constructor(private router: Router, private service: CustomerService) {
    this.updateCartItemsLength();
    this.service.cartUpdated.subscribe(() => {
      this.updateCartItemsLength();
    })
  }
  resolved(captchaResponse: string | null) {
    if (captchaResponse !== null) {
      // Handle the resolved reCAPTCHA response
      console.log(`Resolved captcha with response: ${captchaResponse}`);
    } else {
      // Handle the case when the response is null
      console.error('Null reCAPTCHA response received');
    }
  }
  

  ngOnInit() {
    if (typeof localStorage !== 'undefined') {
      const isAdminLoggedIn = localStorage.getItem('isAdminLoggedIn');
      const isCustomerLoggedIn = localStorage.getItem('iscustomerLogedin');
      if (isAdminLoggedIn === 'true') {
        this.isadminloggedin = true;
        this.iscustomerLogedin = false;
        this.isloggedin = false;
      } else if (isCustomerLoggedIn === 'true') {
        this.isadminloggedin = false;
        this.iscustomerLogedin = true;
        this.isloggedin = false;
      }
      this.emailId = localStorage.getItem('emailId');
    }
  }

  updateCartItemsLength() {
    if (typeof localStorage !== 'undefined') {
      const cartItems = JSON.parse(localStorage.getItem("cartItems") || '[]');
      this.cartItemsLength = cartItems.length;
    }
  }

  openModal() {
    this.isModalOpen = true;
  }

  closeModal() {
    this.isModalOpen = false;
  }

  stopPropagation(event: Event) {
    event.stopPropagation();
  }

  async loginSubmit(loginForm: any) {
    this.isModalOpen = false;
    if (this.captchaResponse && !this.captchaInvalid) {
      // Your login form submission logic here
      console.log('Login form submitted');
    } else {
      // Show error message or handle invalid reCAPTCHA response
      console.error('Invalid reCAPTCHA response');
    }
    if (loginForm.emailId == 'admin' && loginForm.password == 'admin') {
      localStorage.setItem("isAdminLoggedIn", "true");
      localStorage.setItem("iscustomerLogedin", "false");
      localStorage.setItem("isLoggedIn", "false");
      this.isadminloggedin = true;
      this.iscustomerLogedin = false;
      this.isloggedin = false;
      localStorage.setItem("emailId", loginForm.emailId);
      this.router.navigate(['']);
    } else {
      this.cust = null;
      await this.service.customerLogin(loginForm.emailId, loginForm.password).then((data: any) => {
        console.log(data);
        this.cust = data;
      });

      if (this.cust != null) {
        localStorage.setItem("emailId", loginForm.emailId);
        localStorage.setItem("isLoggedIn", "false");
        localStorage.setItem("isAdminLoggedIn", "false");
        localStorage.setItem("iscustomerLogedin", "true");
        this.isadminloggedin = false;
        this.iscustomerLogedin = true;
        this.isloggedin = false;
        this.router.navigate(['']);
      } else {
        alert('Invalid Credentials');
      }
    }
  }

  logout() {
    localStorage.removeItem("isAdminLoggedIn");
    localStorage.removeItem("iscustomerLogedin");
    localStorage.setItem("isLoggedIn", "true");
    this.isadminloggedin = false;
    this.iscustomerLogedin = false;
    this.isloggedin = true;
    this.router.navigate(['']);
    if (typeof window !== 'undefined' && window.localStorage) {
      window.localStorage.removeItem('emailId');
      window.localStorage.clear();
      alert('Successfully logged out');
    } else {
      console.error('localStorage is not available');
    }
  }
}
