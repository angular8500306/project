import { Component, OnInit } from '@angular/core';
import { AdminService } from '../admin.service';

@Component({
  selector: 'app-bookings',
  templateUrl: './bookings.component.html',
  styleUrl: './bookings.component.css'
})
export class BookingsComponent implements OnInit {

  bookings: any;
  emailId: any;

  constructor(private service: AdminService) {
    };

  ngOnInit() {
    this.service.getBookings().subscribe((data: any) => { this.bookings = data; });
  }

  deleteBooking(book: any) {
    this.service.deleteBooking(book.bookingId).subscribe((data: any) => {console.log(data);});

    const i = this.bookings.findIndex((element: any) => {
      return element.bookingId == book.bookingId;
    });

    this.bookings.splice(i, 1);

    alert('Booking Deleted Successfully!!!');
  }
  
}
