import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CustomerService } from '../customer.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrl: './register.component.css'
})
export class RegisterComponent implements OnInit {
  cust: any;

  constructor(private service: CustomerService, private router: Router) {
    this.cust = {
      firstName: '',
      lastName: '',
      address: '',
      gender: '',
      emailId: '',
      phoneNumber: '',
      password: '',
      confirmPassword: ''
    }
  }

  ngOnInit() {
  }

  registerSubmit(regForm: any) {
    console.log(regForm);


    this.cust.firstName = regForm.firstName;
    this.cust.lastName = regForm.lastName;
    this.cust.gender = regForm.gender;
    this.cust.address = regForm.address;
    this.cust.emailId = regForm.emailId;
    this.cust.phoneNumber = regForm.phoneNumber;
    this.cust.password = regForm.password;
    this.cust.confirmPassword = regForm.confirmPassword;

    console.log(this.cust);

    this.service.registerCustomer(this.cust).subscribe((data: any) => { console.log(data); });
    this.router.navigate(['']);
  }

}
