import { Component, OnInit } from '@angular/core';
import { CustomerService } from '../customer.service';

@Component({
  selector: 'app-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.css']
})
export class ServicesComponent implements OnInit {
  services: any[];
  emailId: any;
  cartItems: any[];

  constructor(private service: CustomerService) {
    this.services = [
      {id:1001, name:"Car tow",   price:14999.00, description:"car towing services", imgsrc:"assets/Images/cartowing.jpg", addedToCart: false},
      {id:1002, name:"Battery Jump", price:65464, description:"Battery Jump ", imgsrc:"assets/Images/cartowing.jpg", addedToCart: false}
    ];
    this.cartItems = [];
  }

  ngOnInit() {
    if (typeof localStorage !== 'undefined') {
      this.cartItems = JSON.parse(localStorage.getItem("cartItems") || '[]');
      this.updateAddedToCartStatus();
    }
  }

  addToCart(service: any) {
    if (typeof localStorage !== 'undefined') {
      const existingService = this.cartItems.find(item => item.id === service.id);
      if (existingService) {
        existingService.quantity += 1;
      } else {
        this.cartItems.push({...service, quantity: 1});
        service.addedToCart = true;
      }
      localStorage.setItem("cartItems", JSON.stringify(this.cartItems));
      this.updateAddedToCartStatus();
      this.service.cartUpdated.emit();
    }
  }

  updateAddedToCartStatus() {
    for (let service of this.services) {
      service.addedToCart = !!this.cartItems.find(item => item.id === service.id);
    }
  }
}