import { AfterViewInit, Component, ElementRef } from '@angular/core';

@Component({
  selector: 'app-aboutus',
  templateUrl: './aboutus.component.html',
  styleUrls: ['./aboutus.component.css']
})
export class AboutusComponent implements AfterViewInit {
  constructor(private elementRef: ElementRef) {}

  ngAfterViewInit() {
    const slider = this.elementRef.nativeElement.querySelector('.slider');
    const items = this.elementRef.nativeElement.querySelectorAll('.item');

    if (slider && items.length > 0) {
      this.slide('next'); // Ensure initial display is correct
    }
  }

  slide(direction: string) {
    const slider = this.elementRef.nativeElement.querySelector('.slider');
    const items = this.elementRef.nativeElement.querySelectorAll('.item');
    
    if (slider && items.length > 0) {
      if (direction === 'next') {
        slider.appendChild(items[0]);
      } else if (direction === 'prev') {
        slider.insertBefore(items[items.length - 1], items[0]);
      }
    }
  }
}
