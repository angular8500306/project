import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { CustomerService } from '../customer.service';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css']
})
export class LogoutComponent implements OnInit {
  constructor(private router: Router, private service: CustomerService) {}

  ngOnInit() {
    if (typeof window !== 'undefined' && window.localStorage) {
      window.localStorage.removeItem('emailId');
      window.localStorage.clear();
      alert('Successfully logged out');
      this.router.navigate(['home']);
    } else {
      console.error('localStorage is not available');
    }
  }
}
