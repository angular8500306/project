import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AdminService {

  constructor(private http: HttpClient) { }

  getCustomers(): any {
    return this.http.get('http://localhost:8085/getCustomers');
  }

  deleteCustomer(cust: any) {
    return this.http.delete('http://localhost:8085/deleteCustomerById/' + cust);
  }

  getBookings(): any {
    return this.http.get('http://localhost:8085/getBookings');
  }

  deleteBooking(book: any) {
    return this.http.delete('http://localhost:8085/deleteBookingById/' + book);
  }
}
