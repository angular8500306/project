import { HttpClient } from '@angular/common/http';
import { Injectable ,EventEmitter} from '@angular/core';
import { BehaviorSubject, Observable, catchError, map } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {
  cartUpdated: EventEmitter<any> = new EventEmitter();
  private cartItemsSubject = new BehaviorSubject<any[]>([]);
  cartItems$ = this.cartItemsSubject.asObservable();

  constructor(private http: HttpClient) { }

  updateCartItems(cartItems: any[]) {
    this.cartItemsSubject.next(cartItems);
  }
  registerCustomer(customer: any): any {
    return this.http.post('http://localhost:8085/addCustomer', customer);
  }
  customerLogin(emailId: any, password: any): any {
    return this.http.get('http://localhost:8085/customerLogin/' + emailId + '/' + password).toPromise();
  }

  confirmBooking(booking: any):any{
    return this.http.post('http://localhost:8085/addBooking', booking);
  }
  getLocationName(latitude: number, longitude: number): Observable<string> {
    const url = `https://nominatim.openstreetmap.org/reverse?format=jsonv2&lat=${latitude}&lon=${longitude}`;

    return this.http.get<any>(url).pipe(
      map(response => {
        console.log('API Response:', response); // Log the entire response

        if (response.address) {
          let locationName = '';

          // Add street number if available
          if (response.address.house_number) {
            locationName += 'Street No: ' + response.address.house_number + ', ';
          }

          // Add area
          if (response.address.neighbourhood) {
            locationName += response.address.neighbourhood + ', ';
          } else if (response.address.suburb) {
            locationName += response.address.suburb + ', ';
          }

          // Add city
          if (response.address.city) {
            locationName += response.address.city + ', ';
          }

          // Add state
          if (response.address.state) {
            locationName += response.address.state + ', ';
          }

          // Add pincode if available
          if (response.address.postcode) {
            locationName += 'Pin Code: ' + response.address.postcode;
          }

          return locationName.trim();
        } else {
          throw new Error('Failed to retrieve location name');
        }
      }),
      catchError(error => {
        console.error('Error retrieving location name:', error);
        throw new Error('Failed to retrieve location name');
      })
    );
  }
}