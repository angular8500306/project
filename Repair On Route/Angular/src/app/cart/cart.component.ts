import { Component, OnInit } from '@angular/core';
import { CustomerService } from '../customer.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {
  booking: any;
  location: string = '';
  cartItems: any[];
  emailId: any;
  loggedInUser: any;
  isModalOpen: boolean = false;
  total: number;

  constructor(private service: CustomerService, private router: Router) {

    this.total = 0;
    this.booking = {
      services: '',
      location: '',
      emailId: '',
      phoneNumber: ''
    }

    if (typeof localStorage !== 'undefined') {
      this.cartItems = JSON.parse(localStorage.getItem("cartItems") || '[]');
      this.emailId = localStorage.getItem('emailId');

      this.loggedInUser = JSON.parse(localStorage.getItem("loggedInUser") || '{}');

      if (this.loggedInUser && this.loggedInUser.email) {
        this.emailId = this.loggedInUser.email;
      }

      this.cartItems.forEach((element: any) => {
        this.total += element.price;
      });

    } else {
      this.cartItems = [];
      this.emailId = '';
      this.loggedInUser = {};
    }
  }

  openModal() {
    this.isModalOpen = true;
  }

  closeModal() {
    this.isModalOpen = false;
  }
  stopPropagation(event: Event) {
    event.stopPropagation();
  }
  removeFromCart(index: number) {
    const removedItem = this.cartItems.splice(index, 1)[0];
    this.total -= removedItem.price;
    localStorage.setItem("cartItems", JSON.stringify(this.cartItems));
    this.service.cartUpdated.emit();
  }

  getCurrentLocation() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(
        (position) => {
          const latitude = position.coords.latitude;
          const longitude = position.coords.longitude;
          this.service.getLocationName(latitude, longitude)
            .subscribe(
              (locationName) => {
                this.location = locationName;
              },
              (error) => {
                console.error('Error getting location:', error);
                this.location = 'Unknown';
              }
            );
        },
        (error) => {
          console.error('Error getting location:', error);
          this.location = 'Unknown';
        }
      );
    } else {
      console.error('Geolocation is not supported by this browser.');
      this.location = 'Unavailable';
    }
  }

  book() {
    alert("Conirm Your location")
  }

  bookingSubmit(bookForm: any) {
    this.booking.services = this.cartItems.map(service => service.name).join(', ');
    this.booking.location = this.location;
    this.booking.emailId = this.emailId;
    this.booking.phoneNumber = this.booking.phoneNumber;

    console.log(this.booking);
    console.log(bookForm);

    this.service.confirmBooking(this.booking).subscribe((data: any) => {
      console.log(data);
      // Clear the cart after successfully confirming the booking
      this.cartItems = [];
      this.total = 0;
      localStorage.removeItem("cartItems");
      this.service.cartUpdated.emit();
      alert("Booking confirmed successfully!");
    }, (error: any) => {
      console.error('Error confirming booking:', error);
      alert("Booking failed. Please try again.");
    });
    this.router.navigate(['']);
  }
  ngOnInit() {
  }
}