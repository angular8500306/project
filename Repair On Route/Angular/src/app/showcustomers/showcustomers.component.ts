import { Component, OnInit } from '@angular/core';
import { AdminService } from '../admin.service';

@Component({
  selector: 'app-showcustomers',
  templateUrl: './showcustomers.component.html',
  styleUrl: './showcustomers.component.css'
})


export class ShowcustomersComponent implements OnInit {

  customers: any;
  emailId: any;

  constructor(private service: AdminService) {

    };

  ngOnInit() {
    this.service.getCustomers().subscribe((data: any) => { this.customers = data; });
  }

  deleteCustomer(cust: any) {
    this.service.deleteCustomer(cust.customerId).subscribe((data: any) => {console.log(data);});

    const i = this.customers.findIndex((element: any) => {
      return element.customerId == cust.customerId;
    });

    this.customers.splice(i, 1);

    alert('Customer Deleted Successfully!!!');
  }
}
