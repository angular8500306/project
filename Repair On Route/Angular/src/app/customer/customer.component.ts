import { Component, OnInit } from '@angular/core';
import { CustomerService } from '../customer.service';

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrl: './customer.component.css'
})
export class CustomerComponent implements OnInit {

  emailId: any;
  cartItemsLength: number = 0;

  constructor(private service: CustomerService) {}

  ngOnInit() {
    console.log('ngOnInit() called in CustomerComponent');
    if (typeof localStorage !== 'undefined') {
      this.emailId = localStorage.getItem('emailId');
      const cartItems = JSON.parse(localStorage.getItem("cartItems") || '[]');
      this.cartItemsLength = cartItems.length;
      console.log('Cart Items Length:', this.cartItemsLength); // Print cartItemsLength to console
    }
  }
  
}