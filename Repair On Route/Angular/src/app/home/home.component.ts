import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CustomerService } from '../customer.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrl: './home.component.css'
})
export class HomeComponent implements OnInit {
  isModalOpen: boolean = false;
  cust: any;
  captchaResponse: string = '';

  constructor(private router: Router, private service: CustomerService) {
  }
  openModal() {
    this.isModalOpen = true;
  }

  closeModal() {
    this.isModalOpen = false;
  }
  stopPropagation(event: Event) {
    event.stopPropagation();
  }
  ngOnInit() {
  }

  async loginSubmit(loginForm: any) {
    if (loginForm.emailId == 'admin' && loginForm.password == 'admin') {
      //   //Setting the isUserLoggedIn variable value to true under EmpService
      localStorage.setItem("emailId", loginForm.emailId);
      this.router.navigate(['admin']);
      console.log(loginForm)
    } else {
      this.cust = null;

      await this.service.customerLogin(loginForm.emailId, loginForm.password).then((data: any) => {
        console.log(data);
        this.cust = data;
      });

      if (this.cust != null) {
        localStorage.setItem("emailId", loginForm.emailId);
        console.log(loginForm)
        this.router.navigate(['customer']);
      } else {
        alert('Invalid Credentials');
      }
    }
  }
}




